<?php
/**
 * Extend questionExtraSurvey and reloadAnyResponse to allow usage of LDAP request (with LDAPTokenAuthenticate)
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu <www.sondages.pro>
 * @copyright 2022 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class questionExtraSurveyByLDAP extends PluginBase
{
    protected static $name = 'questionExtraSurveyByLDAP';
    protected static $description = 'Extend questionExtraSurvey and reloadAnyResponse to allow usage of LDAP request.';

    protected $storage = 'DbStorage';

    protected $settings = array(
        'server' => array(
            'type' => 'string',
            'label' => 'LDAP server',
            'help' => 'e.g. ldap://ldap.example.com or ldaps://ldap.example.com'
        ),
        'ldapport' => array(
            'type' => 'string',
            'label' => 'Port number',
            'help' => 'Default when omitted is 389',
        ),
        'ldapversion' => array(
            'type' => 'select',
            'label' => 'LDAP version',
            'options' => array('2' => 'LDAPv2', '3'  => 'LDAPv3'),
            'default' => '2',
        ),
        'ldapoptreferrals' => array(
            'type' => 'boolean',
            'label' => 'Select true if referrals must be followed (use false for ActiveDirectory)',
            'default' => '0'
        ),
        'ldaptls' => array(
            'type' => 'boolean',
            'help' => 'Check to enable Start-TLS encryption, when using LDAPv3',
            'label' => 'Enable Start-TLS',
            'default' => '0'
            ),
        'binddn' => array(
            'type' => 'string',
            'label' => 'DN of the LDAP account used to search for the end-user\'s DN.',
            'help' => 'Optional , an anonymous bind is performed if empty',

        ),
        'bindpwd' => array(
            'type' => 'password',
            'label' => 'Password of the LDAP account used to search for the end-user\'s DN.'
        ),

        'searchattribute' => array(
            'type' => 'string',
            'label' => 'Attribute to use for response id',
            'help' => '',
        ),
        'searchattributeseparator' => array(
            'type' => 'string',
            'label' => 'Separator between survey id and other part',
            'help' => '',
        ),
        'searchbase' => array(
            'type' => 'text',
            'label' => 'Base DN for the search operation',
            'help' => 'Multiple bases may be separated by a new line',
        ),
        'searchfilter' => array(
            'type' => 'string',
            'label' => 'Optional extra LDAP filter',
            'help' => 'Don\'t forget the outmost enclosing parentheses',
        ),
    );

    public function init()
    {
        $this->subscribe('newQuestionAttributes', 'addExtraSurveyAttribute');
        $this->subscribe('questionExtraSurveyGetPreviousReponseCriteria', 'updateCriteria');
        $this->subscribe('ReloadAnyResponseAllowEdit');

        /* Settings */
        $this->subscribe('beforeRALSurveySettings');
        $this->subscribe('newReloadAnyResponseSurveySettings');
    }

    /**
     */
    public function addExtraSurveyAttribute()
    {
        $extraAttributes = array(
            'extraSurveyByLDAP'=> array(
                'types' => 'XT',
                'category' => $this->gT('Extra survey'), // Todo : get translation by questionExtraSurvey
                'sortorder' => 400,
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->gT('Remind to set LDAP access allowed in reloadAnyReponse plugin. LDAP filter on extra right is not apply.'),
                'caption' => $this->gT('Get survey list with LDAP'),
            ),
            //~ 'extraSurveyByLDAPReplace'=> array(
                //~ 'types' => 'XT',
                //~ 'category' => $this->gT('Extra survey'), // Todo : get translation by questionExtraSurvey
                //~ 'sortorder' => 410,
                //~ 'inputtype' => 'singleselect',
                //~ 'options' => array(
                    //~ 'add' => $this->gT("Add restriction"),
                    //~ 'replace' => $this->gT("Replace"),
                //~ ),
                //~ 'default' => 'add',
                //~ 'help' => $this->gT('When reloading this was never tester; except the global filter'), //
                //~ 'caption' => $this->gT('Restrict or replace list of survey'),
            //~ ),
        );
        $this->getEvent()->append('questionAttributes', $extraAttributes);
    }

    /**
     * Update or replace criteria
     */
    public function updateCriteria()
    {
        $CriteriaEvent = $this->getEvent();
        $qid = $CriteriaEvent->get('qid');
        $oAttribute = QuestionAttribute::model()->find(
            "qid = :qid AND attribute = :attribute",
            array(":qid" => $qid, ":attribute" => 'extraSurveyByLDAP')
        );
        if (!$oAttribute or !$oAttribute->value) {
            return;
        }
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $surveyid = $CriteriaEvent->get('surveyId');
        $userLdap = null;
        if (!empty(App()->session['ldapUser'])) {
            $userLdap = App()->session['ldapUser'];
        }
        $settings = $this->getPluginSettings(true);
        $options = array(
            'user' => $userLdap,
            'surveyid' => $surveyid
        );
        $LdapExtraSurvey = new questionExtraSurveyByLDAP\LdapExtraSurvey(
            $settings,
            $options
        );
        $SrIds = $LdapExtraSurvey->getSridList();
        $criteria = $CriteriaEvent->get('criteria');
        if (empty($SrIds)) {
            $SrIds = [0];
        }
        $criteria->addInCondition('id', $SrIds);
    }

    public function beforeRALSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $SurveySettingsEvent->set("settings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'checkLDAPSurvey' => array(
                    'type' => 'select',
                    'label' => $this->gT("Check if user have access on LDAP."),
                    'options' => array(
                        1 => gT("Yes"),
                        0 => gT("No"),
                    ),
                    'current' => $this->get('checkLDAPSurvey', 'Survey', $surveyId, 0)
                ),
                'extraLDAPRestriction' => array(
                    'type' => 'text',
                    'label' => $this->gT("Extra condition for LDAP access."),
                    'default' => '',
                    'current' => $this->get('extraLDAPRestriction', 'Survey', $surveyId, ""),
                    'help' => $this->gT('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>) . '),
                ),
            )
        ));
        
    }
    public function newReloadAnyResponseSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $settings = $SurveySettingsEvent->get('settings');
        foreach($settings as $setting => $value) {
            $this->set($setting, $value, 'Survey', $surveyId);
        }
    }

    public function ReloadAnyResponseAllowEdit()
    {
        $ReloadAnyResponseEvent = $this->getEvent();
        $surveyId = $ReloadAnyResponseEvent->get('surveyId');
        if (!$this->get('checkLDAPSurvey', 'Survey', $surveyId, 0)) {
            return;
        }
        $srid = $ReloadAnyResponseEvent->get('srid');
        /* Check if LDAP restriction is here */
        $extraRestriction = $this->get('extraLDAPRestriction', 'Survey', $surveyId, '');
        if ($extraRestriction) {
            $responseCriteria = \reloadAnyResponse\Utilities::getResponseCriteria($surveyId, $srid, $extraRestriction);
            $responseCriteria->select = array('id');
            $oResponse = SurveyDynamic::model($surveyId)->find($responseCriteria);
            if (empty($oResponse)) {
                return false;
            }
        }
        
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $userLdap = null;
        if (!empty(App()->session['ldapUser'])) {
            $userLdap = App()->session['ldapUser'];
        }
        $settings = $this->getPluginSettings(true);
        $options = array(
            'user' => $userLdap,
            'surveyid' => $surveyId
        );
        $LdapExtraSurvey = new questionExtraSurveyByLDAP\LdapExtraSurvey(
            $settings,
            $options
        );
        $LDAPRightOnSrid = $LdapExtraSurvey->getRightOnSrid($srid);
        if ($LDAPRightOnSrid) {
            $ReloadAnyResponseEvent->set('allowed', true);
        }
    }
    
}
