<?php

/**
 * This file is part of questionExtraSurveyByLDAP plugin
 * @version 0.1.0
 */

namespace questionExtraSurveyByLDAP;

class LdapExtraSurvey
{

    /**
     * the error when try to authenticate
     * @var string
     **/
    public $ErrorInfo = '';

    /**
     * the settings of pplugi for LDAP connexion
     * @var null|array
     **/
    protected $settings;

    /**
     * @param array $settings
     * @param array $surveySettings
     * @param string|null $language
     */
    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    /**
     * Authenticate user with user/pass
     * @return false|array, the list of srid
     */
    public function getSridList()
    {
        // Try to connect
        $ldapconn = $this->createConnection();
        if (!is_resource($ldapconn)) {
            if (empty($this->ErrorInfo)) {
                $this->addError("Unable to create a LDAP connexion.", \CLogger::LEVEL_ERROR);
            }
            return;
        }
        $responseids = array();
        $searchattribute = strtolower($this->getSetting('searchattribute'));
        $searchbase = $this->getSetting('searchbase');
        $searchfilter = $this->getSetting('searchfilter');
        $datasearchfilter = "";
        if ($searchfilter != "") {
            $datasearchfilter = "$searchfilter";
        }

        $searchattributes = array(
            'dn',
            $searchattribute
        );
        $dataentries = array();
        $searchbase = preg_split('/\r\n|\r|\n|;/', $searchbase);
        foreach ($searchbase as $base) {
            $dnsearchres = ldap_search($ldapconn, $base, $datasearchfilter, $searchattributes);
            $entries = ldap_get_entries($ldapconn, $dnsearchres);
            foreach($entries as $entry) {
                if (!empty($entry[$searchattribute])) {
                    if ($entry[$searchattribute]["count"] == 1) {
                        $responseids[] = $entry[$searchattribute][0];
                    } else {
                        $values = $entry[$searchattribute];
                        unset($values['count']);
                        $responseids = array_merge($responseids, $values);
                    }
                }
            }
        }
        /* Fix it */
        $responseids = array_unique(array_filter(array_map(
            function ($responseid) {
                return intval($responseid);
            },
            $responseids
        )));
        
        return $responseids;
    }

    /**
     * Get if current LDAP user have right on srid
     * @return boolean|null
     */
    public function getRightOnSrid($srid)
    {
        // Try to connect
        $ldapconn = $this->createConnection();
        if (!is_resource($ldapconn)) {
            if (empty($this->ErrorInfo)) {
                $this->addError("Unable to create a LDAP connexion.", \CLogger::LEVEL_ERROR);
            }
            return;
        }
        $searchattribute = strtolower($this->getSetting('searchattribute'));
        $searchbase = $this->getSetting('searchbase');
        $searchfilter = $this->getSetting('searchfilter');
        $datasearchfilter = "({$searchattribute}={$srid})";
        if ($searchfilter != "") {
            $datasearchfilter = "(&{$datasearchfilter}{$searchfilter})";
        }
        $searchbase = preg_split('/\r\n|\r|\n|;/', $searchbase);
        foreach ($searchbase as $base) {
            $dnsearchres = ldap_search($ldapconn, $base, $datasearchfilter, [$searchattribute]);
            $entries = ldap_get_entries($ldapconn, $dnsearchres);
            foreach($entries as $entry) {
                if (!empty($entry[$searchattribute]) && $entry[$searchattribute]["count"] > 0) {
                    return true;
                }
            }
        }
        return null;
    }

    /**
     * Create connexion and return resource
     */
    private function createConnection()
    {
        $ldapserver = $this->getSetting('server');
        if (strpos($ldapserver, 'ldaps://') === false && strpos($ldapserver, 'ldap://') === false) {
            $ldapserver = 'ldap://' . $ldapserver;
        }
        $ldapport = $this->getSetting('ldapport');
        if (empty($ldapport)) {
            $ldapport = 389;
        }
        $ldapver = $this->getSetting('ldapversion');
        $ldaptls = $this->getSetting('ldaptls');
        $ldapoptreferrals  = $this->getSetting('ldapoptreferrals');
        $ldapconn = ldap_connect($ldapserver . ':' . (int) $ldapport);
        if (false == $ldapconn) {
            // LDAP connect does not connect, but just checks the URI
            $this->ErrorInfo = gT('LDAP URI could not be parsed.');
            return false;
        }
        if (empty($ldapver)) {
            // If the version hasn't been set, default = 2
            $ldapver = 2;
        }

        $connectionSuccessful = ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, $ldapver);
        if (!$connectionSuccessful) {
            $this->ErrorInfo = gT('Error creating LDAP connection');
            return false;
        }
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, $ldapoptreferrals);
        if (!empty($ldaptls) && $ldaptls == '1' && $ldapver == 3 && preg_match("/^ldaps:\/\//", $ldapserver) === 0) {
            // starting TLS secure layer
            if (!ldap_start_tls($ldapconn)) {
                ldap_unbind($ldapconn); // Could not properly connect, unbind everything.
                $this->ErrorInfo = gT('Error creating TLS on LDAP connection');
                return false;
            }
        }
        $binddn = $this->getSetting('binddn');
        $bindpwd = $this->getSetting('bindpwd');
        if (empty($binddn)) {
            // There is no account defined to do the LDAP search,
            // let's use anonymous bind instead
            $ldapbindsearch = @ldap_bind($ldapconn);
        } else {
            // An account is defined to do the LDAP search, let's use it
            $ldapbindsearch = @ldap_bind($ldapconn, $binddn, $bindpwd);
        }
        if (!$ldapbindsearch) {
            $this->addError(ldap_error($ldapconn), \CLogger::LEVEL_ERROR);
            ldap_close($ldapconn); // all done? close connection
            return false;
        }
        return $ldapconn;
    }

    /**
     * Get a specified settings from Plugin
     * @param string
     * @return string
     */
    private function getSetting($setting)
    {
        if (empty($this->settings[$setting])) {
            return null;
        }
        if (isset($this->settings[$setting]['current'])) {
            return $this->settings[$setting]['current'];
        }
        if (isset($this->settings[$setting]['default'])) {
            return $this->settings[$setting]['default'];
        }
        return null;
    }

    /**
     * set the error when login
     * @param string message
     * @param string level for log
     * @param string function
     * @return void
     */
    private function addError($message, $level = \CLogger::LEVEL_INFO, $function = 'getSridList')
    {
        $this->ErrorInfo = gT($message);
        \Yii::log("Unable to create a LDAP connexion.", $level, 'plugin.questionExtraSurveyByLDAP.LdapAuthentication.' . $function);
    }
}
