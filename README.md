# questionExtraSurveyByLDAP

Access to survey via LDAP request, and extend questionExtraSurvey

## Usage

This plugin need reloadAnyReponse 5.4.0 and use questionExtraSurvey 4.4.0

Plugin settings allow to create filter used by all related LDAP request, reloadAnyReponse settings aloow to set reload, and questionExtraSurvey attribute for listing of LDAP survey.
